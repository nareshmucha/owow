from rest_framework import serializers
from .models import rommates
class rommateserializer(serializers.ModelSerializer):
    class Meta:
        model=rommates
        fields=["firstname","lastname","age","job","designation","current_city","home_city"]
        #fields='__all__'