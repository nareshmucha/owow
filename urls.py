from django.conf.urls import url
from django.contrib import admin
from Covalent3 import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^rommates/',views.rommatesList.as_view()),
]