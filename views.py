# -*- coding: utf-8 -*-
#from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import get_list_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import rommates
from .serializers import rommateserializer

#from django.shortcuts import render

# Create your views here.
class rommatesList(APIView):
    def get(self,request):
        rommates1=rommates.objects.all()
        serializer=rommateserializer(rommates1,many=True)
        return Response(serializer.data)
